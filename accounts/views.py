from django.shortcuts import render
from django.shortcuts import render, redirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login


# Create your views here.
def login_view(request):
    form = LoginForm()
    if request.method == "GET":

        return render(request, "accounts/login.html", {"form": form})
    username = request.POST.get("username")
    password = request.POST.get("password")
    if username and password:
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("list_projects")
        else:
            return render(request, "accounts/login.html", {"form": form})
